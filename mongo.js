// Query Operators

// [SECTION] Comparison Query Operators
// $gt and $gte operator (Greater than/Greater than or equal to)


// find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})

// find users with an age greater than or equal to 50
db.users.find({
	age: {
		$gte: 50
	}
})

// find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})

// find users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})

// find users with an age that is not equal to 82
db.users.find({
	age: {
		$ne: 82
	}
})

// find users whose last names are either "Hawking" or "Doe"
// $in = only one field
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})


// find users whose courses including "HTML" or "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

// [SECTION] Logical Query Operators

// $or operator
// different fields
db.users.find({
	$or: [
	{
		firstName: "Neil"
	},
	{
		age: 21
	}
	]
})

// $and operator
db.users.find({$and: [{age: {$ne:82}}, {age: {$ne:76}}]})

// Inclusion
// - allows us to include/add specific fields only when retrieving documents
// - the value provided is 1 to denote that the field is being included.

// Syntax:
// db.users.find({criteria}, {field: 1})

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1
}
)

// Exclusion
//  - Allows us to exclude/remove specific fields only when retrieving documents
//  the value provided is 1 to denote that the field is being excluded
// Syntax:
//  db.users.find({criteria}, {field: 0})
db.users.find({
	firstName: "Jane"
},
{
	contact: 0,
	department: 0
}
)


// Suppressing the ID field
//  - Allows us to exclude the "_id" field when retrieving documents
//  When using field projection, field inclusion, and exclusion may not be used at the same time

//  Excluding the "_id" field is the only exception to this rule

db.users.find({
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
})

// Returning Specific Fields in Embedded Documents
db.users.find({
	firstName: "Jane"
},{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
})

// Supressing Specific Fields in embedded documents

db.users.find({
	firstName: "Jane"
},{
	"contact.phone": 0
}).pretty();


// Project Specific Array Elements in the Returned Array
// The $slice operator allows us to retrieve
//  only 1 element that matches the search criteria

db.users.find({
	"namearr":{
		name: "Jane"
	}
},{
	namearr: 
	{$slice: 1}
}
)

// $regex operator
// Allows us to find documents that match a specific string pattern using regular expressions

// Syntax
// db.users.find({field: $regex: 'pattern', $options: '$optionValue'})

// Case sensitive query
db.users.find({firstName: {$regex: 'N'}}).pretty();

// Case insensitive query $i
db.users.find({firstName: {$regex: 'j', $options: '$i'}})


db.users.find({
	$or: [
	{firstName: {$regex: 's', $options: '$i'}},
	{lastName: {$regex: 'd', $options: '$i'}}
	]
}, {firstName: 1, lastName: 1, _id: 0}).pretty();




